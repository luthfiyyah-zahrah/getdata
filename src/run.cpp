#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Imu.h>

void chatterCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
  ROS_INFO("Imu Seq: [%d]", msg->header.seq);
  ROS_INFO("Imu Orientation x: [%f], y: [%f], z: [%f], w: [%f]", msg->orientation.x,msg->orientation.y,msg->orientation.z,msg->orientation.w);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "go_three_second");
  ros::NodeHandle nh;

  ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("RosAria/cmd_vel", 1000);
  geometry_msgs::Twist msg;

  double BASE_SPEED = 0.2, MOVE_TIME = 3.0, CLOCK_SPEED = 0.5;
  int count = 0;
  ros::Rate rate(CLOCK_SPEED);

  // Make the robot stop (robot perhaps has a speed already)
  msg.linear.x = 0;
  msg.linear.y = 0;
  msg.linear.z = 0;
  msg.angular.x = 0;
  msg.angular.y = 0;
  
  msg.angular.z = 0;
  pub.publish(msg);
  int a,b;
  std::cout<<std::endl<<"input speed and time 1.300,2.400,3.500 4.10s,5.15s,6.20s"<<std::endl;
      std::cin>>a;
      std::cin>>b;
        
  if (a==1){
    BASE_SPEED=0.3;
  }else if(a==2){
    BASE_SPEED=0.4;
  }else if(a==3){
    BASE_SPEED=0.5;
  }else{
    std::cout<<"please choose"<<std::endl;
  }
  if(b==4){
    MOVE_TIME=10.0;
  }else if(b==5){
    MOVE_TIME=15.0;
  }else if(b==6){
    MOVE_TIME=20.0;
  }else{
    std::cout<<"please choose"<<std::endl;
  }
  while(ros::ok() && count<MOVE_TIME/CLOCK_SPEED + 1)
    {
      if (count == 0 || count == 1)
  {
      msg.linear.x = BASE_SPEED;
      pub.publish(msg);
      ros::Subscriber sub = nh.subscribe("/TinyCube/imu/raw", 1000, chatterCallback);
  }
      count++;
      ros::spinOnce();
      rate.sleep();
   }
  
  // make the robot stop
  for (int i = 0; i < 2; i++)
    {  
      msg.linear.x = 0;
      msg.linear.y = 0;
      msg.linear.z = 0;
      pub.publish(msg);
    }
    ROS_INFO("The robot finished moving forward [%f] seconds!",MOVE_TIME);
    
    // make sure the robot stops.
    rate.sleep();
   

}
